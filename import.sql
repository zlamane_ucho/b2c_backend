CREATE SCHEMA IF NOT EXISTS b2c_db ;

CREATE TABLE IF NOT EXISTS b2c_db.University(
  id VARCHAR(500) NOT NULL PRIMARY KEY,
  additionDate DATETIME,
  name VARCHAR(40),
  address VARCHAR(255),
  email VARCHAR(255),
  phone VARCHAR(30)
);

CREATE TABLE IF NOT EXISTS b2c_db.FieldOfStudy(
  universityId VARCHAR(500),
  fieldId VARCHAR(255),
  startDate DATETIME,
  studentsLimit INT,
  constraint PK_FieldOfStudy primary key(universityId, fieldId)
);

CREATE TABLE IF NOT EXISTS b2c_db.Student(
  id VARCHAR(500) primary key,
  firstName VARCHAR(40),
  lastName VARCHAR(40),
  email VARCHAR(40),
  phone VARCHAR(40),
  age INT,
  gender ENUM('male', 'female'),
  studentStatus ENUM('active', 'inactive', 'suspended', 'dean\'s leave'),
  averageGrade NUMERIC(4,3)
);

CREATE TABLE IF NOT EXISTS b2c_db.StudentsGrades(
  id VARCHAR(500) primary key,
  studentId VARCHAR(500),
  fieldId VARCHAR(255),
  grade NUMERIC(4,3)
);

CREATE TABLE IF NOT EXISTS b2c_db.StudentsOnUniversity(
  id VARCHAR(500),
  studentId VARCHAR(500),
  universityId VARCHAR(500)
);
COMMIT;
