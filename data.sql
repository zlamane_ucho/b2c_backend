/*insert universities*/
insert into b2c_db.university(id,addition_date,name,address,email,phone) 
values ("1", "2019-03-28", "Politechnika Śląska", "Akademicka 2, 44-100 Gliwice", "ps@ps.pl", "777-323-323")
on duplicate key update 
addition_date="2019-03-28",
name="Politechnika Śląska",
address="Akademicka 2, 44-100 Gliwice",
email="ps@ps.pl",
phone="777-323-323";

insert into b2c_db.university(id,addition_date,name,address,email,phone) 
values ("2", "2019-03-28", "Universytet Śląski", "Kościuszki 57, 40170 Katowice", "us@us.pl", "777-323-323")
on duplicate key update 
addition_date="2019-03-28",
name="Universytet Śląski",
address="Kościuszki 57, 40170 Katowice",
email="us@us.pl",
phone="777-323-323";

insert into b2c_db.university(id,addition_date,name,address,email,phone) 
values ("3", "2019-03-28", "Universytet Jagieloński", "Pszczyńska 120, 53-102 Kraków", "uj@uj.pl", "777-323-323")
on duplicate key update 
addition_date="2019-03-28",
name="Universytet Jagieloński",
address="Pszczyńska 120, 53-102 Kraków",
email="uj@uj.pl",
phone="777-323-323";

/*insert fields*/
insert into b2c_db.field_of_study(id,university_id,name_of_study_field,start_date, students_limit) 
values ("1", "1", "Physics", "2019-10-01", 25)
on duplicate key update students_limit = 25;

insert into b2c_db.field_of_study(id,university_id,name_of_study_field,start_date, students_limit) 
values ("2", "1", "Social Sciences", "2019-10-01", 25)
on duplicate key update students_limit = 25;

insert into b2c_db.field_of_study(id,university_id,name_of_study_field,start_date, students_limit) 
values ("3", "1", "Computer Programming", "2019-10-01", 25)
on duplicate key update students_limit = 25;

insert into b2c_db.field_of_study(id,university_id,name_of_study_field,start_date, students_limit) 
values ("4", "1", "Digital Circuit", "2019-10-01", 25)
on duplicate key update students_limit = 25;

insert into b2c_db.field_of_study(id,university_id,name_of_study_field,start_date, students_limit) 
values ("5", "1", "Circuit Theory", "2019-10-01", 25)
on duplicate key update students_limit = 25;

insert into b2c_db.field_of_study(id,university_id,name_of_study_field,start_date, students_limit) 
values ("6", "2", "Optimization", "2019-10-01", 25)
on duplicate key update students_limit = 25;

insert into b2c_db.field_of_study(id,university_id,name_of_study_field,start_date, students_limit) 
values ("7", "2", "Systen Dynamics", "2019-10-01", 25)
on duplicate key update students_limit = 25;

insert into b2c_db.field_of_study(id,university_id,name_of_study_field,start_date, students_limit) 
values ("8", "2", "Signal Processing", "2019-10-01", 25)
on duplicate key update students_limit = 25;

insert into b2c_db.field_of_study(id,university_id,name_of_study_field,start_date, students_limit) 
values ("9", "2", "Logic Programming", "2019-10-01", 25)
on duplicate key update students_limit = 25;

insert into b2c_db.field_of_study(id,university_id,name_of_study_field,start_date, students_limit) 
values ("10", "2", "Artificial Inteligence", "2019-10-01", 25)
on duplicate key update students_limit = 25;

insert into b2c_db.field_of_study(id,university_id,name_of_study_field,start_date, students_limit) 
values ("11", "3", "Discrete Maths", "2019-10-01", 25)
on duplicate key update students_limit = 25;

insert into b2c_db.field_of_study(id,university_id,name_of_study_field,start_date, students_limit) 
values ("12", "3", "Numerical Methods", "2019-10-01", 25)
on duplicate key update students_limit = 25;

insert into b2c_db.field_of_study(id,university_id,name_of_study_field,start_date, students_limit) 
values ("13", "3", "Probability", "2019-10-01", 25)
on duplicate key update students_limit = 25;

insert into b2c_db.field_of_study(id,university_id,name_of_study_field,start_date, students_limit) 
values ("14", "3", "Conrol Fundamentals", "2019-10-01", 25)
on duplicate key update students_limit = 25;

insert into b2c_db.field_of_study(id,university_id,name_of_study_field,start_date, students_limit) 
values ("15", "3", "Computer Networks", "2019-10-01", 25)
on duplicate key update students_limit = 25;

/*insert students, students grades, student uni*/
/*1*/
insert into b2c_db.student(id,first_name,last_name,email, phone, age, gender, student_status, average_grade) 
values ("1", "Adrian", "Nowak", "student@mail.com", "734-342-233", 20, "MALE", "ACTIVE", 4.0)
on duplicate key update phone = "734-342-233";

insert into b2c_db.students_on_university(id, student_id, university_id)
values("1", "1", "1")
on duplicate key update university_id = "1";

insert into b2c_db.students_grades(id, student_id, field_id, grade)
values("1", "1", "1", 4.0)
on duplicate key update student_id = "1";
insert into b2c_db.students_grades(id, student_id, field_id, grade)
values("2", "1", "2", 3.0)
on duplicate key update student_id = "1";
insert into b2c_db.students_grades(id, student_id, field_id, grade)
values("3", "1", "3", 5.0)
on duplicate key update student_id = "1";
/*2*/
insert into b2c_db.student(id,first_name,last_name,email, phone, age, gender, student_status, average_grade) 
values ("2", "Przmysław", "Grucha", "student@mail.com", "734-342-233", 24, "MALE", "INACTIVE", 4.0)
on duplicate key update phone = "734-342-233";

insert into b2c_db.students_on_university(id, student_id, university_id)
values("2", "2", "1")
on duplicate key update university_id = "1";

insert into b2c_db.students_grades(id, student_id, field_id, grade)
values("4", "2", "1", 4.0)
on duplicate key update student_id = "2";
insert into b2c_db.students_grades(id, student_id, field_id, grade)
values("5", "2", "3", 3.5)
on duplicate key update student_id = "2";
insert into b2c_db.students_grades(id, student_id, field_id, grade)
values("6", "2", "5", 4.5)
on duplicate key update student_id = "2";
/*3*/
insert into b2c_db.student(id,first_name,last_name,email, phone, age, gender, student_status, average_grade) 
values ("3", "Zofia", "Zguba", "student@mail.com", "734-342-233", 22, "FEMALE", "ACTIVE", 3.83)
on duplicate key update phone = "734-342-233";

insert into b2c_db.students_on_university(id, student_id, university_id)
values("3", "3", "1")
on duplicate key update university_id = "1";

insert into b2c_db.students_grades(id, student_id, field_id, grade)
values("7", "3", "2", 3.5)
on duplicate key update student_id = "3";
insert into b2c_db.students_grades(id, student_id, field_id, grade)
values("9", "3", "3", 3.5)
on duplicate key update student_id = "3";
insert into b2c_db.students_grades(id, student_id, field_id, grade)
values("9", "3", "4", 4.5)
on duplicate key update student_id = "3";
/*4*/
insert into b2c_db.student(id,first_name,last_name,email, phone, age, gender, student_status, average_grade) 
values ("4", "Agnieszka", "Bratek", "student@mail.com", "734-342-233", 21, "FEMALE", "SUSPENDED", 4.5)
on duplicate key update phone = "734-342-233";

insert into b2c_db.students_on_university(id, student_id, university_id)
values("4", "4", "2")
on duplicate key update university_id = "2";

insert into b2c_db.students_grades(id, student_id, field_id, grade)
values("10", "4", "6", 4.5)
on duplicate key update student_id = "4";
insert into b2c_db.students_grades(id, student_id, field_id, grade)
values("11", "4", "7", 4.5)
on duplicate key update student_id = "4";
insert into b2c_db.students_grades(id, student_id, field_id, grade)
values("12", "4", "8", 4.5)
on duplicate key update student_id = "4";

/*5*/
insert into b2c_db.student(id,first_name,last_name,email, phone, age, gender, student_status, average_grade) 
values ("5", "Piotr", "Kowalski", "student@mail.com", "734-342-233", 21, "MALE", "DEANS_LEAVE", 4.33)
on duplicate key update phone = "734-342-233";

insert into b2c_db.students_on_university(id, student_id, university_id)
values("5", "5", "2")
on duplicate key update university_id = "2";

insert into b2c_db.students_grades(id, student_id, field_id, grade)
values("13", "5", "6", 3.5)
on duplicate key update student_id = "5";
insert into b2c_db.students_grades(id, student_id, field_id, grade)
values("14", "5", "8", 4.5)
on duplicate key update student_id = "5";
insert into b2c_db.students_grades(id, student_id, field_id, grade)
values("15", "5", "10", 5.0)
on duplicate key update student_id = "5";

/*6*/
insert into b2c_db.student(id,first_name,last_name,email, phone, age, gender, student_status, average_grade) 
values ("6", "Jadwiga", "Nocoń", "student@mail.com", "734-342-233", 21, "FEMALE", "ACTIVE", 3.83)
on duplicate key update phone = "734-342-233";

insert into b2c_db.students_on_university(id, student_id, university_id)
values("6", "6", "2")
on duplicate key update university_id = "2";

insert into b2c_db.students_grades(id, student_id, field_id, grade)
values("16", "6", "7", 3.5)
on duplicate key update student_id = "6";
insert into b2c_db.students_grades(id, student_id, field_id, grade)
values("17", "6", "9", 4.0)
on duplicate key update student_id = "6";
insert into b2c_db.students_grades(id, student_id, field_id, grade)
values("18", "6", "10", 4.0)
on duplicate key update student_id = "6";

/*7*/
insert into b2c_db.student(id,first_name,last_name,email, phone, age, gender, student_status, average_grade) 
values ("7", "Marcin", "Kot", "student@mail.com", "734-342-233", 21, "MALE", "ACTIVE", 3.67)
on duplicate key update phone = "734-342-233";

insert into b2c_db.students_on_university(id, student_id, university_id)
values("7", "7", "3")
on duplicate key update university_id = "3";

insert into b2c_db.students_grades(id, student_id, field_id, grade)
values("19", "7", "11", 3.5)
on duplicate key update student_id = "7";
insert into b2c_db.students_grades(id, student_id, field_id, grade)
values("20", "7", "12", 4.0)
on duplicate key update student_id = "7";
insert into b2c_db.students_grades(id, student_id, field_id, grade)
values("21", "7", "13", 3.5)
on duplicate key update student_id = "7";

/*8*/
insert into b2c_db.student(id,first_name,last_name,email, phone, age, gender, student_status, average_grade) 
values ("8", "Zygmunt", "Stary", "student@mail.com", "734-342-233", 21, "MALE", "DEANS_LEAVE", 3.5)
on duplicate key update phone = "734-342-233";

insert into b2c_db.students_on_university(id, student_id, university_id)
values("8", "8", "3")
on duplicate key update university_id = "3";

insert into b2c_db.students_grades(id, student_id, field_id, grade)
values("22", "8", "11", 3.5)
on duplicate key update student_id = "8";
insert into b2c_db.students_grades(id, student_id, field_id, grade)
values("23", "8", "13", 4.0)
on duplicate key update student_id = "8";
insert into b2c_db.students_grades(id, student_id, field_id, grade)
values("24", "8", "15", 3.0)
on duplicate key update student_id = "8";

/*9*/
insert into b2c_db.student(id,first_name,last_name,email, phone, age, gender, student_status, average_grade) 
values ("9", "Paulina", "Gwóźdź", "student@mail.com", "734-342-233", 21, "FEMALE", "ACTIVE", 4.17)
on duplicate key update phone = "734-342-233";

insert into b2c_db.students_on_university(id, student_id, university_id)
values("9", "9", "3")
on duplicate key update university_id = "3";

insert into b2c_db.students_grades(id, student_id, field_id, grade)
values("25", "9", "12", 5.0)
on duplicate key update student_id = "9";
insert into b2c_db.students_grades(id, student_id, field_id, grade)
values("26", "9", "14", 4.5)
on duplicate key update student_id = "9";
insert into b2c_db.students_grades(id, student_id, field_id, grade)
values("27", "9", "15", 3.0)
on duplicate key update student_id = "9";
