CREATE SCHEMA IF NOT EXISTS b2c_db ;
USE b2c_db;

CREATE TABLE IF NOT EXISTS b2c_db.university(
  id VARCHAR(500) NOT NULL PRIMARY KEY,
  addition_date DATETIME,
  name VARCHAR(40),
  address VARCHAR(255),
  email VARCHAR(255),
  phone VARCHAR(30)
);

CREATE TABLE IF NOT EXISTS b2c_db.field_of_study(
  id VARCHAR(500) NOT NULL PRIMARY KEY,
  university_id VARCHAR(500),
  name_of_study_field VARCHAR(255),
  start_date DATETIME,
  students_limit INT
);

CREATE TABLE IF NOT EXISTS b2c_db.student(
  id VARCHAR(500) primary key,
  first_name VARCHAR(40),
  last_name VARCHAR(40),
  email VARCHAR(40),
  phone VARCHAR(40),
  age INT,
  gender ENUM('MALE', 'FEMALE'),
  student_status ENUM('ACTIVE', 'INACTIVE', 'SUSPENDED', 'DEANS_LEAVE'),
  average_grade NUMERIC(4,3)
);

CREATE TABLE IF NOT EXISTS b2c_db.students_grades(
  id VARCHAR(500) primary key,
  student_id VARCHAR(500),
  field_id VARCHAR(255),
  grade NUMERIC(4,3)
);

CREATE TABLE IF NOT EXISTS b2c_db.students_on_university(
  id VARCHAR(500) PRIMARY KEY,
  student_id VARCHAR(500),
  university_id VARCHAR(500)
);
COMMIT;
