package com.example.timothy.backend.entities;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="field_of_study")
public class FieldOfStudy {

	@Id @GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid", strategy = "uuid")
	private String id;
	@Column(name="university_id")
	private String universityId;
	@Column(name="name_of_study_field")
	private String nameOfStudyField;
	@Column(name="start_date")
	private Date startDate;
	@Column(name="students_limit")
	private Integer studentsLimit;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUniversityId() {
		return universityId;
	}

	public void setUniversityId(String universityId) {
		this.universityId = universityId;
	}

	public String getNameOfStudyField() {
		return nameOfStudyField;
	}

	public void setNameOfStudyField(String nameOfStudyField) {
		this.nameOfStudyField = nameOfStudyField;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Integer getStudentsLimit() {
		return studentsLimit;
	}

	public void setStudentsLimit(Integer studentsLimit) {
		this.studentsLimit = studentsLimit;
	}

	@Override
	public String toString() {
		return "FieldOfStudy{" +
				"id='" + id + '\'' +
				", universityId='" + universityId + '\'' +
				", nameOfStudyField='" + nameOfStudyField + '\'' +
				", startDate=" + startDate +
				", studentsLimit=" + studentsLimit +
				'}';
	}
}
