package com.example.timothy.backend.entities;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.context.annotation.Bean;

import javax.persistence.*;

@Entity
@Table(name="students_on_university")
public class StudentsOnUniversity {
	@Id
	@GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid", strategy = "uuid")
	private String id;
	@Column(name="student_id")
	private String studentId;
	@Column(name="university_id")
	private String universityId;

	public StudentsOnUniversity() {
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getStudentId() {
		return studentId;
	}

	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}

	public String getUniversityId() {
		return universityId;
	}

	public void setUniversityId(String universityId) {
		this.universityId = universityId;
	}

	@Override
	public String toString() {
		return "StudentsOnUniversity{" +
				"id='" + id + '\'' +
				", studentId='" + studentId + '\'' +
				", universityId='" + universityId + '\'' +
				'}';
	}
}
