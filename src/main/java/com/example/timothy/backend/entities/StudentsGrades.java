package com.example.timothy.backend.entities;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name="students_grades")
public class StudentsGrades {

	@Id
	@GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid", strategy = "uuid")
	private String id;
	@Column(name="student_id")
	private String studentId;
	@Column(name="field_id")
	private String fieldId;
	private Double grade;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getStudentId() {
		return studentId;
	}

	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}

	public String getFieldId() {
		return fieldId;
	}

	public void setFieldId(String fieldId) {
		this.fieldId = fieldId;
	}

	public Double getGrade() {
		return grade;
	}

	public void setGrade(Double grade) {
		this.grade = grade;
	}

	@Override
	public String toString() {
		return "StudentsGrades{" +
				"id='" + id + '\'' +
				", studentId='" + studentId + '\'' +
				", fieldId='" + fieldId + '\'' +
				", grade=" + grade +
				'}';
	}
}
