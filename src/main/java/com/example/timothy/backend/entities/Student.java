package com.example.timothy.backend.entities;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name="student")
public class Student {

	public enum Gender{
		BUFFER("buffer"), MALE("male"), FEMALE("female");
		private String name;

		private Gender(String name) {this.name = name;}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}
	}
	public enum Status{
		BUFFER("buffer"), ACTIVE("active"), INACTIVE("inactive"), SUSPENDED("suspended"), DEANS_LEAVE("dean's leave");
		private String name;

		private Status(String name) {
			this.name = name;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}
	}

	@Id @GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid", strategy = "uuid")
	private String id;
	@Column(name="first_name")
	private String firstName;
	@Column(name="last_name")
	private String lastName;
	private String email;
	private String phone;
	private Integer age;
	@Enumerated(EnumType.STRING)
	private Gender gender;
	@Column(name="student_status")
	@Enumerated(EnumType.STRING)
	private Status studentStatus;
	@Column(name="average_grade")
	private Double averageGrade;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public Status getStudentStatus() {
		return studentStatus;
	}

	public void setStudentStatus(Status studentStatus) {
		this.studentStatus = studentStatus;
	}

	public Double getAverageGrade() {
		return averageGrade;
	}

	public void setAverageGrade(Double averageGrade) {
		this.averageGrade = averageGrade;
	}

	@Override
	public String toString() {
		return "Student{" +
				"id='" + id + '\'' +
				", firstName='" + firstName + '\'' +
				", lastName='" + lastName + '\'' +
				", email='" + email + '\'' +
				", phone='" + phone + '\'' +
				", age=" + age +
				", gender=" + gender +
				", studentStatus=" + studentStatus +
				", averageGrade=" + averageGrade +
				'}';
	}
}