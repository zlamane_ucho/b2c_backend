package com.example.timothy.backend.controllers;


import com.example.timothy.backend.entities.FieldOfStudy;
import com.example.timothy.backend.entities.StudentsGrades;
import com.example.timothy.backend.entities.StudentsOnUniversity;
import com.example.timothy.backend.entities.University;
import com.example.timothy.backend.repositories.*;
import com.example.timothy.backend.responses.DeleteResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.Optional;

@Controller
@RequestMapping("/university")
public class UniversityController {

	@Autowired
	private UniversityRepo universityRepo;
	@Autowired
	private FieldOfStudyRepo fieldOfStudyRepo;
	@Autowired
	private StudentsGradesRepo studentsGradesRepo;
	@Autowired
	private StudentRepo studentRepo;
	@Autowired
	private StudentsOnUniversityRepo studentsOnUniversityRepo;

	@GetMapping(path = "/all")
	public @ResponseBody
	Iterable<University> getAllUniversities() {
		return universityRepo.findAll();
	}

	@PostMapping(path = "/add")
	public @ResponseBody University addUniversity(@RequestBody University university){
		return universityRepo.save(university);
	}

	@GetMapping(path = "/{id}")
	public @ResponseBody
	Optional<University> getById(@PathVariable("id")String id){
		return universityRepo.findById(id);
	}

	@GetMapping(path = "/countStudents/{id}", produces = "application/json")
	public @ResponseBody
	Integer countStudentsOnUniversity(@PathVariable String id){
		return studentsOnUniversityRepo.countAllByUniversityId(id);
	}
	@GetMapping(path = "/countFields/{id}", produces = "application/json")
	public @ResponseBody
	Integer countFieldsOnUniversity(@PathVariable String id){
		return fieldOfStudyRepo.countAllByUniversityId(id);
	}

	@DeleteMapping(path = "/delete/{id}")
	@Transactional
	public @ResponseBody
	DeleteResponse deleteUniversity(@PathVariable String id){
		//Delete university
		universityRepo.deleteById(id);
		//Delete fields on this university
		Iterable<FieldOfStudy> fieldsOfStudy = fieldOfStudyRepo.findByUniversityId(id);
		fieldOfStudyRepo.deleteByUniversityId(id);
		//Delete grades for deleted fields or for deleted students
		fieldsOfStudy.forEach(field -> studentsGradesRepo.deleteByFieldId(field.getId()));
		//Delete students on university by university id
		Iterable<StudentsOnUniversity> studentsOnUniversities = studentsOnUniversityRepo.getByUniversityId(id);
		studentsOnUniversityRepo.deleteByUniversityId(id);
		//Delete students on this university
		studentsOnUniversities.forEach(student -> studentRepo.deleteById(student.getStudentId()));
		return new DeleteResponse(true);
	}

}
