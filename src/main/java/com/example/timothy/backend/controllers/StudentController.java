package com.example.timothy.backend.controllers;

import com.example.timothy.backend.entities.Student;
import com.example.timothy.backend.entities.StudentsGrades;
import com.example.timothy.backend.entities.StudentsOnUniversity;
import com.example.timothy.backend.entities.University;
import com.example.timothy.backend.repositories.StudentRepo;
import com.example.timothy.backend.repositories.StudentsGradesRepo;
import com.example.timothy.backend.repositories.StudentsOnUniversityRepo;
import com.example.timothy.backend.responses.DeleteResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;

@Controller
@RequestMapping("/student")
public class StudentController {

	@Autowired
	private StudentRepo studentRepo;
	@Autowired
	private StudentsOnUniversityRepo studentsOnUniversityRepo;
	@Autowired
	private StudentsGradesRepo studentsGradesRepo;

	private StudentsOnUniversity studentsOnUniversity = new StudentsOnUniversity();

	@PostMapping(path = "/add/{uniId}")
	public @ResponseBody
	Student addStudent(@RequestBody Student student, @PathVariable String uniId){
		studentsOnUniversity.setUniversityId(uniId);
		Student savedStudent = studentRepo.save(student);
		studentsOnUniversity.setStudentId(student.getId());
		studentsOnUniversityRepo.save(studentsOnUniversity);
		return savedStudent;
	}

	@PostMapping(path = "/update")
	public @ResponseBody
	Student updateStudent(@RequestBody Student student){
		return studentRepo.save(student);
	}

	@GetMapping(path = "/newAvgGrade/{id}/{avg}")
	public @ResponseBody
	Student updateAverageGrade(@PathVariable(name="id") String id, @PathVariable(name="avg") Double avg){
		Student student = studentRepo.findById(id).get();
		student.setAverageGrade(avg);
		return studentRepo.save(student);
	}

	@GetMapping(path = "/all")
	public @ResponseBody
	Iterable<Student> getAllStudents(){
		return studentRepo.findAll();
	}

	@DeleteMapping(path = "/delete/{id}", produces="application/json")
	@Transactional
	public @ResponseBody
	DeleteResponse deleteStudent(@PathVariable String id){
		studentRepo.deleteById(id);
		studentsGradesRepo.deleteByStudentId(id);
		studentsOnUniversityRepo.deleteByStudentId(id);
		return new DeleteResponse(true);
	}

}
