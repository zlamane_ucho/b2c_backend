package com.example.timothy.backend.controllers;

import com.example.timothy.backend.entities.*;
import com.example.timothy.backend.repositories.FieldOfStudyRepo;
import com.example.timothy.backend.repositories.StudentRepo;
import com.example.timothy.backend.repositories.StudentsGradesRepo;
import com.example.timothy.backend.repositories.StudentsOnUniversityRepo;
import com.example.timothy.backend.responses.DeleteResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.Optional;


@Controller
@RequestMapping("/fieldofstudy")
public class FieldOfStudyController {

	@Autowired
	private FieldOfStudyRepo fieldOfStudyRepo;
	@Autowired
	private StudentsGradesRepo studentsGradesRepo;
	@Autowired
	private StudentRepo studentRepo;
	@Autowired
	private StudentsOnUniversityRepo studentsOnUniversityRepo;

	@GetMapping(path = "/all")
	public @ResponseBody
	Iterable<FieldOfStudy> getAllFields() {
		// This returns a JSON or XML with the users
		return fieldOfStudyRepo.findAll();
	}
	@GetMapping(path="/universityId/{id}")
	public @ResponseBody Iterable<FieldOfStudy> getByUniversityId(@PathVariable("id")String id){
		return fieldOfStudyRepo.findByUniversityIdOrderByNameOfStudyField(id);
	}
	@GetMapping(path = "/possiblefields/{id}")
	public  @ResponseBody
	Iterable<FieldOfStudy> getPossibleFields (@PathVariable String id){
		StudentsOnUniversity studentOnUniversity = studentsOnUniversityRepo.getByStudentId(id);
		return fieldOfStudyRepo.findByUniversityId(studentOnUniversity.getUniversityId());
	}
	@PostMapping(path = "/add")
	public @ResponseBody Iterable<FieldOfStudy> addField(@RequestBody Iterable<FieldOfStudy> fieldOfStudy){
		return fieldOfStudyRepo.saveAll(fieldOfStudy);
	}
	@DeleteMapping(path = "delete/{id}", produces = "application/json")
	@Transactional
	public @ResponseBody
	DeleteResponse deleteField(@PathVariable String id){
		fieldOfStudyRepo.deleteById(id);

		//Update students average grade
		Iterable<StudentsGrades> studentsToUpdate = studentsGradesRepo.findByFieldId(id);
		studentsGradesRepo.deleteByFieldId(id);
		studentsToUpdate.forEach(student -> {
			String studentId = student.getStudentId();
			double sumOfGrades = 0;
			double fieldsCount = 0;
			Iterable<StudentsGrades> studentGrades = studentsGradesRepo.findByStudentId(studentId);
			for(StudentsGrades grade: studentGrades){
				System.out.println("Get grade: " +grade.getGrade());
				sumOfGrades += grade.getGrade();
				fieldsCount++;
			}
			double averageGrade;
			if(fieldsCount == 0) averageGrade = 0.0;
			else averageGrade = Math.round(sumOfGrades * 100.0 / fieldsCount) /100.0;

			Student studentToUpdate = studentRepo.findById(studentId).get();
			studentToUpdate.setAverageGrade(averageGrade);

			studentRepo.save(studentToUpdate);
		});

		return new DeleteResponse(true);
	}
}
