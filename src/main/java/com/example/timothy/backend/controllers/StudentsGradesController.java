package com.example.timothy.backend.controllers;

import com.example.timothy.backend.entities.StudentsGrades;
import com.example.timothy.backend.repositories.StudentsGradesRepo;
import com.example.timothy.backend.responses.DeleteResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;

@Controller
@RequestMapping("/studentsGrades")
public class StudentsGradesController {
	@Autowired
	private StudentsGradesRepo studentsGradesRepo;


	@PostMapping("/add")
	public  @ResponseBody
	Iterable<StudentsGrades> addStudentGrades(@RequestBody Iterable<StudentsGrades> studentsGrades){
		return studentsGradesRepo.saveAll(studentsGrades);
	}
	@GetMapping("/studentfields/{id}")
	public @ResponseBody
	Iterable<StudentsGrades> getStudentFields(@PathVariable String id){
		return studentsGradesRepo.findByStudentId(id);
	}
	@DeleteMapping("/delete/{id}")
	@Transactional
	public @ResponseBody
	DeleteResponse deleteByStudentsId(@PathVariable String id){
		studentsGradesRepo.deleteByStudentId(id);
		return new DeleteResponse(true);
	}
}
