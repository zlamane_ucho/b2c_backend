package com.example.timothy.backend.repositories;

import com.example.timothy.backend.entities.University;
import org.springframework.data.repository.CrudRepository;


public interface UniversityRepo extends CrudRepository<University, String> {
	Iterable<University> findByNameContains(String partName);
}
