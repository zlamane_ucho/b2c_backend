package com.example.timothy.backend.repositories;

import com.example.timothy.backend.entities.StudentsOnUniversity;
import org.springframework.data.repository.CrudRepository;

public interface StudentsOnUniversityRepo extends CrudRepository<StudentsOnUniversity, String> {
	void deleteByStudentId(String id);

	Iterable<StudentsOnUniversity> getByUniversityId(String id);
	StudentsOnUniversity getByStudentId(String id);
	Integer countAllByUniversityId(String id);

	void deleteByUniversityId(String id);
}
