package com.example.timothy.backend.repositories;

import com.example.timothy.backend.entities.FieldOfStudy;
import org.springframework.data.repository.CrudRepository;

public interface FieldOfStudyRepo extends CrudRepository<FieldOfStudy, String> {
	Iterable<FieldOfStudy> findByUniversityId(String universityId);
	Iterable<FieldOfStudy> findByUniversityIdOrderByNameOfStudyField(String universityId);
	Integer countAllByUniversityId(String id);
	void deleteByUniversityId(String id);
}
