package com.example.timothy.backend.repositories;

import com.example.timothy.backend.entities.Student;
import org.springframework.data.repository.CrudRepository;

public interface StudentRepo extends CrudRepository<Student, String> {
}
