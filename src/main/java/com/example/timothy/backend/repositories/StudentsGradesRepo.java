package com.example.timothy.backend.repositories;

import com.example.timothy.backend.entities.StudentsGrades;
import org.springframework.data.repository.CrudRepository;

public interface StudentsGradesRepo extends CrudRepository<StudentsGrades, String> {
	void deleteByStudentId(String id);
	void deleteByFieldId(String id);
	Iterable<StudentsGrades> findByFieldId(String id);
	Iterable<StudentsGrades> findByStudentId(String id);
}
