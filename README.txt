Tools needed: 
-git 
(frontend)
-node.js 
-npm
(backend)
-java 1.8
-mysql server
-mysql workbench

(backend)
1. open console and cd into/some/folder/to/clone/repo
2. git clone https://zlamane_ucho@bitbucket.org/zlamane_ucho/b2c_backend.git
3. prepare user with grants in sql command window or in MySQL Workbench with sql script
GRANT ALL PRIVILEGES ON b2c_db.* TO 'username'@'localhost' IDENTIFIED BY 'password';
4. open file application.properties (projFolder > src > main > resources) and change credentials to your username and password
5. cd into/project/folder (folder with pom.xml)
6. type "mvn spring-boot:run"
7. to initialize data you can use sql script data.sql (in main backend folder) (open MySQL Workbench -> File -> Open Sql script)

(frontend)
STEPS:
1. open console and cd into/some/folder/to/clone/repo
2. git clone https://zlamane_ucho@bitbucket.org/zlamane_ucho/b2c_frontend.git
3. cd into/project/folder
4. type "npm install" and wait until process is done
5. type "npm start" then go to your browser and type "localhost:3000"